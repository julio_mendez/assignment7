package main.java.assign.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "meetings")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meetings {

    private List<String> meeting = null;
    private String count = null;

    public Meetings() {
        meeting = new ArrayList<String>();
    }

    public List<String> getMeetingList() {
        return meeting;
    }

    public void setMeetingList(List<String> meeting) {
        this.meeting = meeting;
    }

    public void addToMeetingList(String m) {
        this.meeting.add(m);
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
    	if(count.equals(null)) {
    		count = "0";
    	}
        this.count = count;
    }
    
	public String toString() {
		String output = "Meetings: \n";
		for(String project : this.meeting) {
			output += "\t" + project + "\n";
		}
		output += "Count: " + count;
		return output;
	}
}
