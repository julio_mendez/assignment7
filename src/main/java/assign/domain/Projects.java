package main.java.assign.domain; 

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "projects")
@XmlAccessorType(XmlAccessType.FIELD)
public class Projects {

	private Map<String, String> project = null;

	public Map<String, String> getProjects() {
		return project;
	}

	public void setProjects(Map<String, String> projects) {
		this.project = projects;
	}
	
	public String toString() {
		String output = "Projects:\t\t\tNumber of Meetings\n";
		for(String project : this.project.keySet()) {
			output += "" + project;
			for(int i = project.length() - 1; i < 45; i++) {
				output += " ";
			}
			output += "\t" + this.project.get(project) + "\n";
		}
		return output;
	}
}