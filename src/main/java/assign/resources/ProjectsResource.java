package main.java.assign.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import main.java.assign.domain.Projects;
import main.java.assign.services.EavesdropService;

@Path("/myeavesdrop")
public class ProjectsResource {

	EavesdropService eavesdropService;
	

	public ProjectsResource() {
		this.eavesdropService = new EavesdropService();
	}
        
    @GET
    @Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
	    System.out.println("Hello World");
	    return "Hello World!";
	}

	@GET
	@Path("/projects")
	@Produces("text/html")
	public String getAllProjects() throws Exception {
		final Projects projects = new Projects();
		System.out.println("Getting Projects\n");
		projects.setProjects(eavesdropService.getProjectsData());
		String output = projects.toString();
		System.out.println(output);
		output = output.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
		output = output.replace(" ", "&nbsp;");
		output = output.replace("\n", "<br>");
		return output;
	}
}
