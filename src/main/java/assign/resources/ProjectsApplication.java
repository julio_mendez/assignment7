package main.java.assign.resources;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import main.java.assign.resources.ProjectsResource;

@ApplicationPath("/assignment7")
public class ProjectsApplication extends Application {

	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();
	
	public ProjectsApplication() {
	}

	@Override
	public Set<Class<?>> getClasses() {
		classes.add(ProjectsResource.class);
		return classes;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

}