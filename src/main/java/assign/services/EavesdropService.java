package main.java.assign.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class EavesdropService {
	
	public Map<String, String> getProjectsData() {
		Map<String, String> data = new TreeMap<String, String>();
		List<String> allProjects = new ArrayList<String>();
		List<String> allMeetings = new ArrayList<String>();
		
		try {
			getData(allProjects, "http://eavesdrop.openstack.org/meetings");
			for(String project : allProjects) {
				allMeetings = getMeetingData(project);
				data.put(project, "" + allMeetings.size());
			}
			
		    
		} catch (Exception exp) {
	
		exp.printStackTrace();	
		return null;
		}
		
		return data;
	}
	
	public List<String> getMeetingData(String team_name) {
		List<String> meetings = new ArrayList<String>();
		List<String> allYears = new ArrayList<String>();
		List<String> allMeetings= new ArrayList<String>();
		
		
		try {
			getData(allYears, "http://eavesdrop.openstack.org/meetings/" + team_name);
			System.out.println("Getting Data from http://eavesdrop.openstack.org/meetings/" + team_name);
			for(String year : allYears) {
				getData(meetings, "http://eavesdrop.openstack.org/meetings/" + team_name + "/" + year);
				allMeetings = parseMeetings(meetings);
			}
			
		    
		} catch (Exception exp) {
	
		exp.printStackTrace();	
		return null;
		}
		
		return allMeetings;
	}
	
	private void getData(List<String> allData, String source) {
		String data;
		try {
		    Document doc = Jsoup.connect(source).get();
		    Elements links = doc.select("body a");
		    
		    ListIterator<Element> iter = links.listIterator();
		    int counter = 0;
		    while(iter.hasNext()) {
		    	Element e = (Element) iter.next();
		    	if(counter >= 5) {
		    		data = e.html();
		    		data = data.substring(0, data.length() - 1);
		    		allData.add(data);
		    	}
		    	counter++;
		    }
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	
	private List<String> parseMeetings(List<String> meetings) {
		List<String> uniqueMeetings = new ArrayList<String>();
		String meetingName;
		
		for(String meetingLog : meetings) {
			
			int occurrence = 0;
			int pos2 = 0;
			for(int i = 0; i < meetingLog.length(); i++) {
				if(meetingLog.charAt(i) == '.') {
					occurrence++;
				}
				switch(occurrence) {
					case 4:
						pos2 = i;
						occurrence++;
						break;
					default:
						break;
				}
			}
			meetingName = meetingLog.substring(0, pos2);
			
			if(!uniqueMeetings.contains((String) meetingName) && meetingName.length() > 1) {
				uniqueMeetings.add(meetingName);
			}
			
		}
		return uniqueMeetings;
	}

}